<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * QuesttionaireQuestionsFixture
 */
class QuesttionaireQuestionsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id_questionnaire' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_question' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        '_indexes' => [
            'id_questionnaire' => ['type' => 'index', 'columns' => ['id_questionnaire'], 'length' => []],
            'id_question' => ['type' => 'index', 'columns' => ['id_question'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'questtionaire_questions_ibfk_1' => ['type' => 'foreign', 'columns' => ['id_questionnaire'], 'references' => ['questtionaire', 'id_questionnaire'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'questtionaire_questions_ibfk_2' => ['type' => 'foreign', 'columns' => ['id_question'], 'references' => ['questions', 'id_question'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id_questionnaire' => 1,
                'id_question' => 1,
                'id' => 1
            ],
        ];
        parent::init();
    }
}
