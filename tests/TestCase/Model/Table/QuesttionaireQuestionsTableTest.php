<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuesttionaireQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuesttionaireQuestionsTable Test Case
 */
class QuesttionaireQuestionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\QuesttionaireQuestionsTable
     */
    public $QuesttionaireQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.QuesttionaireQuestions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('QuesttionaireQuestions') ? [] : ['className' => QuesttionaireQuestionsTable::class];
        $this->QuesttionaireQuestions = TableRegistry::getTableLocator()->get('QuesttionaireQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuesttionaireQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
