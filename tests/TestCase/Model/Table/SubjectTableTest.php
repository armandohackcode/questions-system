<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubjectTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubjectTable Test Case
 */
class SubjectTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SubjectTable
     */
    public $Subject;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Subject'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Subject') ? [] : ['className' => SubjectTable::class];
        $this->Subject = TableRegistry::getTableLocator()->get('Subject', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Subject);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
