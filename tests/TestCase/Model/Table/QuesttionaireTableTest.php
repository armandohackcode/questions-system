<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuesttionaireTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuesttionaireTable Test Case
 */
class QuesttionaireTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\QuesttionaireTable
     */
    public $Questtionaire;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Questtionaire',
        'app.Questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Questtionaire') ? [] : ['className' => QuesttionaireTable::class];
        $this->Questtionaire = TableRegistry::getTableLocator()->get('Questtionaire', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Questtionaire);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
