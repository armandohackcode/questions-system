<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Subject Controller
 *
 * @property \App\Model\Table\SubjectTable $Subject
 *
 * @method \App\Model\Entity\Subject[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubjectController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $subject = $this->paginate($this->Subject);

        $this->set(compact('subject'));
    }

    /**
     * View method
     *
     * @param string|null $id Subject id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subject = $this->Subject->get($id, [
            'contain' => []
        ]);

        $this->set('subject', $subject);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subject = $this->Subject->newEntity();
        if ($this->request->is('post')) {
            $subject = $this->Subject->patchEntity($subject, $this->request->getData());
            if ($this->Subject->save($subject)) {
                $this->Flash->success(__('The {0} has been saved.', 'Subject'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Subject'));
        }
        $this->set(compact('subject'));
    }


    /**
     * Edit method
     *
     * @param string|null $id Subject id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subject = $this->Subject->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subject = $this->Subject->patchEntity($subject, $this->request->getData());
            if ($this->Subject->save($subject)) {
                $this->Flash->success(__('The {0} has been saved.', 'Subject'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Subject'));
        }
        $this->set(compact('subject'));
    }


    /**
     * Delete method
     *
     * @param string|null $id Subject id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subject = $this->Subject->get($id);
        if ($this->Subject->delete($subject)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Subject'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Subject'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
