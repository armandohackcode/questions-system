<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * QuesttionaireQuestions Controller
 *
 * @property \App\Model\Table\QuesttionaireQuestionsTable $QuesttionaireQuestions
 *
 * @method \App\Model\Entity\QuesttionaireQuestion[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QuesttionaireQuestionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $questtionaireQuestions = $this->paginate($this->QuesttionaireQuestions);

        $this->set(compact('questtionaireQuestions'));
    }

    /**
     * View method
     *
     * @param string|null $id Questtionaire Question id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $questtionaireQuestion = $this->QuesttionaireQuestions->get($id, [
            'contain' => []
        ]);

        $this->set('questtionaireQuestion', $questtionaireQuestion);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $questtionaireQuestion = $this->QuesttionaireQuestions->newEntity();
        if ($this->request->is('post')) {
            $questtionaireQuestion = $this->QuesttionaireQuestions->patchEntity($questtionaireQuestion, $this->request->getData());
            if ($this->QuesttionaireQuestions->save($questtionaireQuestion)) {
                $this->Flash->success(__('The questtionaire question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The questtionaire question could not be saved. Please, try again.'));
        }
        $this->set(compact('questtionaireQuestion'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Questtionaire Question id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $questtionaireQuestion = $this->QuesttionaireQuestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $questtionaireQuestion = $this->QuesttionaireQuestions->patchEntity($questtionaireQuestion, $this->request->getData());
            if ($this->QuesttionaireQuestions->save($questtionaireQuestion)) {
                $this->Flash->success(__('The questtionaire question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The questtionaire question could not be saved. Please, try again.'));
        }
        $this->set(compact('questtionaireQuestion'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Questtionaire Question id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $questtionaireQuestion = $this->QuesttionaireQuestions->get($id);
        if ($this->QuesttionaireQuestions->delete($questtionaireQuestion)) {
            $this->Flash->success(__('The questtionaire question has been deleted.'));
        } else {
            $this->Flash->error(__('The questtionaire question could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
