<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Questtionaire Controller
 *
 * @property \App\Model\Table\QuesttionaireTable $Questtionaire
 *
 * @method \App\Model\Entity\Questtionaire[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QuesttionaireController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $questtionaire = $this->paginate($this->Questtionaire);

        $this->set(compact('questtionaire'));
    }

    /**
     * View method
     *
     * @param string|null $id Questtionaire id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $questtionaire = $this->Questtionaire->get($id, [
            'contain' => ['Questions']
        ]);

        $this->set('questtionaire', $questtionaire);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $questtionaire = $this->Questtionaire->newEntity();
        if ($this->request->is('post')) {
            $questtionaire = $this->Questtionaire->patchEntity($questtionaire, $this->request->getData());
            if ($this->Questtionaire->save($questtionaire)) {
                $this->Flash->success(__('The {0} has been saved.', 'Questtionaire'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Questtionaire'));
        }
        $questions = $this->Questtionaire->Questions->find('list', ['limit' => 200]);
        $this->set(compact('questtionaire', 'questions'));
    }


    /**
     * Edit method
     *
     * @param string|null $id Questtionaire id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $questtionaire = $this->Questtionaire->get($id, [
            'contain' => ['Questions']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $questtionaire = $this->Questtionaire->patchEntity($questtionaire, $this->request->getData());
            if ($this->Questtionaire->save($questtionaire)) {
                $this->Flash->success(__('The {0} has been saved.', 'Questtionaire'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Questtionaire'));
        }
        $questions = $this->Questtionaire->Questions->find('list', ['limit' => 200]);
        $this->set(compact('questtionaire', 'questions'));
    }


    /**
     * Delete method
     *
     * @param string|null $id Questtionaire id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $questtionaire = $this->Questtionaire->get($id);
        if ($this->Questtionaire->delete($questtionaire)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Questtionaire'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Questtionaire'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
