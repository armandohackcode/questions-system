<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Questtionaire Model
 *
 * @property \App\Model\Table\QuestionsTable&\Cake\ORM\Association\BelongsToMany $Questions
 *
 * @method \App\Model\Entity\Questtionaire get($primaryKey, $options = [])
 * @method \App\Model\Entity\Questtionaire newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Questtionaire[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Questtionaire|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Questtionaire saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Questtionaire patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Questtionaire[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Questtionaire findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class QuesttionaireTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('questtionaire');
        $this->setDisplayField('id_questionnaire');
        $this->setPrimaryKey('id_questionnaire');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Questions', [
            'foreignKey' => 'questtionaire_id',
            'targetForeignKey' => 'question_id',
            'joinTable' => 'questtionaire_questions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_questionnaire')
            ->allowEmptyString('id_questionnaire', null, 'create');

        $validator
            ->dateTime('start_date')
            ->requirePresence('start_date', 'create')
            ->notEmptyDateTime('start_date');

        $validator
            ->dateTime('end_date')
            ->allowEmptyDateTime('end_date');

        $validator
            ->time('duration_time')
            ->allowEmptyTime('duration_time');

        $validator
            ->boolean('public')
            ->requirePresence('public', 'create')
            ->notEmptyString('public');

        $validator
            ->scalar('password')
            ->maxLength('password', 15)
            ->allowEmptyString('password');

        $validator
            ->boolean('show_answers')
            ->requirePresence('show_answers', 'create')
            ->notEmptyString('show_answers');

        $validator
            ->integer('repetitions')
            ->notEmptyString('repetitions');

        $validator
            ->numeric('total_score')
            ->requirePresence('total_score', 'create')
            ->notEmptyString('total_score');

        $validator
            ->integer('id_subject')
            ->requirePresence('id_subject', 'create')
            ->notEmptyString('id_subject');

        $validator
            ->integer('id_user')
            ->requirePresence('id_user', 'create')
            ->notEmptyString('id_user');

        return $validator;
    }
}
