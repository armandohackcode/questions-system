<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QuesttionaireQuestions Model
 *
 * @method \App\Model\Entity\QuesttionaireQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\QuesttionaireQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QuesttionaireQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QuesttionaireQuestion|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuesttionaireQuestion saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuesttionaireQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QuesttionaireQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QuesttionaireQuestion findOrCreate($search, callable $callback = null, $options = [])
 */
class QuesttionaireQuestionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('questtionaire_questions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_questionnaire')
            ->requirePresence('id_questionnaire', 'create')
            ->notEmptyString('id_questionnaire');

        $validator
            ->integer('id_question')
            ->requirePresence('id_question', 'create')
            ->notEmptyString('id_question');

        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }
}
