<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Questtionaire Entity
 *
 * @property int $id_questionnaire
 * @property \Cake\I18n\FrozenTime $start_date
 * @property \Cake\I18n\FrozenTime|null $end_date
 * @property \Cake\I18n\FrozenTime|null $duration_time
 * @property bool $public
 * @property string|null $password
 * @property bool $show_answers
 * @property int $repetitions
 * @property float $total_score
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $id_subject
 * @property int $id_user
 *
 * @property \App\Model\Entity\Question[] $questions
 */
class Questtionaire extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'start_date' => true,
        'end_date' => true,
        'duration_time' => true,
        'public' => true,
        'password' => true,
        'show_answers' => true,
        'repetitions' => true,
        'total_score' => true,
        'created' => true,
        'modified' => true,
        'id_subject' => true,
        'id_user' => true,
        'questions' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
