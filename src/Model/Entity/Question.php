<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Question Entity
 *
 * @property int $id_question
 * @property float|null $score
 * @property string $question
 * @property \Cake\I18n\FrozenTime $create_at
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $id_category
 *
 * @property \App\Model\Entity\Questtionaire[] $questtionaire
 */
class Question extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'score' => true,
        'question' => true,
        'create_at' => true,
        'modified' => true,
        'id_category' => true,
        'questtionaire' => true
    ];
}
