<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question $question
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Question'), ['action' => 'edit', $question->id_question]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Question'), ['action' => 'delete', $question->id_question], ['confirm' => __('Are you sure you want to delete # {0}?', $question->id_question)]) ?> </li>
        <li><?= $this->Html->link(__('List Questions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Question'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Questtionaire'), ['controller' => 'Questtionaire', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Questtionaire'), ['controller' => 'Questtionaire', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="questions view large-9 medium-8 columns content">
    <h3><?= h($question->id_question) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id Question') ?></th>
            <td><?= $this->Number->format($question->id_question) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($question->score) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Category') ?></th>
            <td><?= $this->Number->format($question->id_category) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Create At') ?></th>
            <td><?= h($question->create_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($question->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Question') ?></h4>
        <?= $this->Text->autoParagraph(h($question->question)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Questtionaire') ?></h4>
        <?php if (!empty($question->questtionaire)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id Questionnaire') ?></th>
                <th scope="col"><?= __('Start Date') ?></th>
                <th scope="col"><?= __('End Date') ?></th>
                <th scope="col"><?= __('Duration Time') ?></th>
                <th scope="col"><?= __('Public') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Show Answers') ?></th>
                <th scope="col"><?= __('Repetitions') ?></th>
                <th scope="col"><?= __('Total Score') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Id Subject') ?></th>
                <th scope="col"><?= __('Id User') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($question->questtionaire as $questtionaire): ?>
            <tr>
                <td><?= h($questtionaire->id_questionnaire) ?></td>
                <td><?= h($questtionaire->start_date) ?></td>
                <td><?= h($questtionaire->end_date) ?></td>
                <td><?= h($questtionaire->duration_time) ?></td>
                <td><?= h($questtionaire->public) ?></td>
                <td><?= h($questtionaire->password) ?></td>
                <td><?= h($questtionaire->show_answers) ?></td>
                <td><?= h($questtionaire->repetitions) ?></td>
                <td><?= h($questtionaire->total_score) ?></td>
                <td><?= h($questtionaire->created) ?></td>
                <td><?= h($questtionaire->modified) ?></td>
                <td><?= h($questtionaire->id_subject) ?></td>
                <td><?= h($questtionaire->id_user) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Questtionaire', 'action' => 'view', $questtionaire->id_questionnaire]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Questtionaire', 'action' => 'edit', $questtionaire->id_questionnaire]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Questtionaire', 'action' => 'delete', $questtionaire->id_questionnaire], ['confirm' => __('Are you sure you want to delete # {0}?', $questtionaire->id_questionnaire)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
