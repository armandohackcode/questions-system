<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QuesttionaireQuestion[]|\Cake\Collection\CollectionInterface $questtionaireQuestions
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Questtionaire Question'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="questtionaireQuestions index large-9 medium-8 columns content">
    <h3><?= __('Questtionaire Questions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id_questionnaire') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id_question') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($questtionaireQuestions as $questtionaireQuestion): ?>
            <tr>
                <td><?= $this->Number->format($questtionaireQuestion->id_questionnaire) ?></td>
                <td><?= $this->Number->format($questtionaireQuestion->id_question) ?></td>
                <td><?= $this->Number->format($questtionaireQuestion->id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $questtionaireQuestion->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $questtionaireQuestion->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $questtionaireQuestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $questtionaireQuestion->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
