<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QuesttionaireQuestion $questtionaireQuestion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Questtionaire Questions'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="questtionaireQuestions form large-9 medium-8 columns content">
    <?= $this->Form->create($questtionaireQuestion) ?>
    <fieldset>
        <legend><?= __('Add Questtionaire Question') ?></legend>
        <?php
            echo $this->Form->control('id_questionnaire');
            echo $this->Form->control('id_question');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
