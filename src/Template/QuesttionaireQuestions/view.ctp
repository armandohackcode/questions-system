<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QuesttionaireQuestion $questtionaireQuestion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Questtionaire Question'), ['action' => 'edit', $questtionaireQuestion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Questtionaire Question'), ['action' => 'delete', $questtionaireQuestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $questtionaireQuestion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Questtionaire Questions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Questtionaire Question'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="questtionaireQuestions view large-9 medium-8 columns content">
    <h3><?= h($questtionaireQuestion->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id Questionnaire') ?></th>
            <td><?= $this->Number->format($questtionaireQuestion->id_questionnaire) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Question') ?></th>
            <td><?= $this->Number->format($questtionaireQuestion->id_question) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($questtionaireQuestion->id) ?></td>
        </tr>
    </table>
</div>
