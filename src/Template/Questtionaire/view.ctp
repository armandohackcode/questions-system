<section class="content-header">
  <h1>
    Questtionaire
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Password') ?></dt>
            <dd><?= h($questtionaire->password) ?></dd>
            <dt scope="row"><?= __('Id Questionnaire') ?></dt>
            <dd><?= $this->Number->format($questtionaire->id_questionnaire) ?></dd>
            <dt scope="row"><?= __('Repetitions') ?></dt>
            <dd><?= $this->Number->format($questtionaire->repetitions) ?></dd>
            <dt scope="row"><?= __('Total Score') ?></dt>
            <dd><?= $this->Number->format($questtionaire->total_score) ?></dd>
            <dt scope="row"><?= __('Id Subject') ?></dt>
            <dd><?= $this->Number->format($questtionaire->id_subject) ?></dd>
            <dt scope="row"><?= __('Id User') ?></dt>
            <dd><?= $this->Number->format($questtionaire->id_user) ?></dd>
            <dt scope="row"><?= __('Start Date') ?></dt>
            <dd><?= h($questtionaire->start_date) ?></dd>
            <dt scope="row"><?= __('End Date') ?></dt>
            <dd><?= h($questtionaire->end_date) ?></dd>
            <dt scope="row"><?= __('Duration Time') ?></dt>
            <dd><?= h($questtionaire->duration_time) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($questtionaire->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($questtionaire->modified) ?></dd>
            <dt scope="row"><?= __('Public') ?></dt>
            <dd><?= $questtionaire->public ? __('Yes') : __('No'); ?></dd>
            <dt scope="row"><?= __('Show Answers') ?></dt>
            <dd><?= $questtionaire->show_answers ? __('Yes') : __('No'); ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Questions') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($questtionaire->questions)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id Question') ?></th>
                    <th scope="col"><?= __('Score') ?></th>
                    <th scope="col"><?= __('Question') ?></th>
                    <th scope="col"><?= __('Create At') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col"><?= __('Id Category') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($questtionaire->questions as $questions): ?>
              <tr>
                    <td><?= h($questions->id_question) ?></td>
                    <td><?= h($questions->score) ?></td>
                    <td><?= h($questions->question) ?></td>
                    <td><?= h($questions->create_at) ?></td>
                    <td><?= h($questions->modified) ?></td>
                    <td><?= h($questions->id_category) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'Questions', 'action' => 'view', $questions->id_question], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'Questions', 'action' => 'edit', $questions->id_question], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'Questions', 'action' => 'delete', $questions->id_question], ['confirm' => __('Are you sure you want to delete # {0}?', $questions->id_question), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
