<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Questtionaire

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('id_questionnaire') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('start_date') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('end_date') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('duration_time') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('public') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('show_answers') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('repetitions') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('total_score') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('id_subject') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('id_user') ?></th>
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($questtionaire as $questtionaire): ?>
                <tr>
                  <td><?= $this->Number->format($questtionaire->id_questionnaire) ?></td>
                  <td><?= h($questtionaire->start_date) ?></td>
                  <td><?= h($questtionaire->end_date) ?></td>
                  <td><?= h($questtionaire->duration_time) ?></td>
                  <td><?= h($questtionaire->public) ?></td>
                  <td><?= h($questtionaire->password) ?></td>
                  <td><?= h($questtionaire->show_answers) ?></td>
                  <td><?= $this->Number->format($questtionaire->repetitions) ?></td>
                  <td><?= $this->Number->format($questtionaire->total_score) ?></td>
                  <td><?= h($questtionaire->created) ?></td>
                  <td><?= h($questtionaire->modified) ?></td>
                  <td><?= $this->Number->format($questtionaire->id_subject) ?></td>
                  <td><?= $this->Number->format($questtionaire->id_user) ?></td>
                  <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $questtionaire->id_questionnaire], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $questtionaire->id_questionnaire], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $questtionaire->id_questionnaire], ['confirm' => __('Are you sure you want to delete # {0}?', $questtionaire->id_questionnaire), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>