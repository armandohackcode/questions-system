<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Questtionaire $questtionaire
 */
?>
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Questtionaire
      <small><?php echo __('Edit'); ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Form'); ?></h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php echo $this->Form->create($questtionaire, ['role' => 'form']); ?>
            <div class="box-body">
              <?php
                echo $this->Form->control('start_date');
                echo $this->Form->control('end_date', ['empty' => true]);
                echo $this->Form->control('duration_time', ['empty' => true]);
                echo $this->Form->control('public');
                echo $this->Form->control('password');
                echo $this->Form->control('show_answers');
                echo $this->Form->control('repetitions');
                echo $this->Form->control('total_score');
                echo $this->Form->control('id_subject');
                echo $this->Form->control('id_user');
                echo $this->Form->control('questions._ids', ['options' => $questions]);
              ?>
            </div>
            <!-- /.box-body -->

          <?php echo $this->Form->submit(__('Submit')); ?>

          <?php echo $this->Form->end(); ?>
        </div>
        <!-- /.box -->
      </div>
  </div>
  <!-- /.row -->
</section>
